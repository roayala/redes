var express=require('express');
var router = express.Router();
var bicicletaController =require('../../controlers/api/bicicletaControllerAPI');

router.get('/',bicicletaController.bicicleta_list);
router.get('/create',bicicletaController.bicicleta_create);
router.get('/delete',bicicletaController.bicicleta_delete);


module.exports = router;