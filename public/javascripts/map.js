var mymap = L.map('mapid').setView([-32.9587076,-60.6609365], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoicm9zYXJpb2F5YWxhIiwiYSI6ImNrZ3praXFxMTA2YXUyeXJpMDFlZjBiMjUifQ.CAsk3bJ0r4UfItOFoFu9jw'
}).addTo(mymap);

var marker = L.marker([-32.938000, -60.651581]).addTo(mymap);
var marker = L.marker([-32.9647164,-60.6268777]).addTo(mymap);
var marker = L.marker([-32.9667934,-60.6257097],{title: 1}).addTo(mymap);
var marker = L.marker([-32.9667934,-60.6257097],{title: 2}).addTo(mymap);


$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicleta.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    }
})